<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

/**
 * Class UserController
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @param User $user
     * @param Request $request
     * @return false|string
     */
    public function store(User $user, Request $request)
    {
        $country = file_get_contents('https://ipapi.co/' . $request->getClientIp() . '/country_name');
        $city = file_get_contents('https://ipapi.co/' . $request->getClientIp() . '/city');

        $weather = $this->getWeather($city);

        foreach ($request->get('clients') as $client) {
            if (isset($client['name']) && isset($client['phone'])) {
                $user->create([
                    'name' => $client['name'],
                    'phone' => $client['phone'],
                    'ip' => $request->getClientIp(),
                    'country' => $country,
                    'weather' => $weather
                ]);
            }
        }

        return json_encode(['status' => 201]);
    }

    protected function getWeather($city)
    {
        return file_get_contents('http://api.openweathermap.org/data/2.5/weather?q='.$city.'&appid='.env('WEATHER_KEY'));
    }
}
