require('./bootstrap');

window.Vue = require('vue');

Vue.component('vue-form', require('./components/VueForm.vue').default);

const app = new Vue({
    el: '#app'
});
