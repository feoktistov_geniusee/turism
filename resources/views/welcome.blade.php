<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script>
        window.App = '{{ json_encode(['csrfToken' => csrf_token() ]) }}';
    </script>

    <!-- Site Properties -->
    <title>@yield('title')</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
</head>
<body>

<div class="ui container segment" id="app">
    <div class="row">
        <div class="col-3"></div>
        <div class="col-6">
            <div class="header text-center">
                <h3>Please, fill out the form</h3>
            </div>
            <vue-form></vue-form>
        </div>
        <div class="col-3"></div>
    </div>
</div>

<script src="{{ asset('js/app.js') }}"></script>

</body>

</html>


